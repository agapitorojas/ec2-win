resource "aws_instance" "ec2_tmp" {
  ami                         = data.aws_ami.winserver2022.id
  instance_type               = var.type
  subnet_id                   = data.aws_subnet.subnet.id
  vpc_security_group_ids      = ["${aws_security_group.sg.id}"]
  key_name                    = aws_key_pair.key.id
  iam_instance_profile        = "AmazonSSMRoleForInstancesQuickSetup"
  associate_public_ip_address = false
  tags                        = local.tags
  volume_tags                 = local.tags
}