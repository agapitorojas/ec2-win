output "ec2_id_ip" {
  value = "${aws_instance.ec2_tmp.id} ${aws_instance.ec2_tmp.private_ip}"
}