resource "aws_key_pair" "key" {
  key_name   = "key_tmp"
  public_key = file(var.pub_key)
}