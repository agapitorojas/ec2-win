locals {
  tags = {
    ef_ambiente                = "dev"
    ef_backup                  = "sem_backup"
    ef_centro_de_custo         = "public_cloud"
    ef_departamento            = "public_cloud"
    ef_iac                     = "terraform"
    ef_plataforma              = "ec2_tmp"
    ef_produto                 = "ec2_tmp"
    ef_projeto                 = "public_cloud"
    ef_provedor                = "aws"
    ef_recuperacao_de_desastre = "nao"
    ef_regiao                  = var.region
    ef_shared                  = "sim"
    ef_torre                   = "public_cloud"
    Name                       = "ec2-tmp"
  }
}